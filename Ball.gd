extends KinematicBody2D

export var start_speed := 800
export var paddle_angle_factor := 10

var velocity := Vector2.ZERO


func _ready():
	velocity.y = -start_speed


func _physics_process(_delta):
	var _v = move_and_slide(velocity, Vector2.UP)
	
	if not break_brick():
		if is_on_wall():
			velocity.x = -velocity.x
			$Wall.play()
			
		if is_on_ceiling():
			velocity.y = -velocity.y
			$Wall.play()
		
		if is_on_floor():
			velocity.y = -start_speed
			$Paddle.play()
			for slide in get_slide_count():
				var collision = get_slide_collision(slide)
				# This should just be the paddle.
				velocity.x = (transform.origin.x - collision.collider.position.x + rand_range(-0.1, 0.1)) * paddle_angle_factor 
	
	velocity = (velocity.normalized() * start_speed)


func break_brick() -> bool:
	for slide in get_slide_count():
		var collision := get_slide_collision(slide)
		if collision.collider is Brick:
			start_speed += (collision.collider.row / 4.0) * get_parent().speed_multiplier
			collision.collider.break_brick()
			
			if is_on_wall():
				velocity.x = -velocity.x
			
			if is_on_ceiling() or is_on_floor():
				velocity.y = -velocity.y

			$Wall.play()
			return true
	
	# if there were no collisions
	return false
