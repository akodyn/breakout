extends CanvasLayer


export var speed_factor = 10
var ball_prefab = preload("res://Ball.tscn")
var bricks_prefab = preload("res://Bricks.tscn")
var ball
var ball_default_pos := Vector2(800, 475)
var score = 0
var current_level = 0
var lives_remaining = 3
var speed_multiplier := 0.0
var game_over = false
onready var bricks = $Bricks


func level_end():
	bricks.queue_free()
	yield(get_tree().create_timer(1.0), "timeout")
	current_level += 1
	
	# If you pass, you get three lives for the next level.
	lives_remaining = 3
	draw_lives_remaining()
	
	bricks = bricks_prefab.instance()
	add_child(bricks)
	bricks.connect("brick_broke", self, "_on_Bricks_brick_broke")
	bricks.connect("last_brick_broke", self, "_on_Bricks_last_brick_broke")

	$UI/Level.text = str(current_level)
	$Player1.reset_ball()
	change_speed_multiplier()


func _ready():
	randomize()


func add_score(value: int):
	score += value
	$UI/Score.text = str(score)


func draw_lives_remaining():
	if lives_remaining == 3:
		$UI/Lives_Remaining.text = "III"
	elif lives_remaining == 2:
		$UI/Lives_Remaining.text = "II "
	elif lives_remaining == 1:
		$UI/Lives_Remaining.text = "I  "
	elif lives_remaining == 0:
		$UI/Lives_Remaining.text = ""
	else:
		$UI/Lives_Remaining.text = "Game Over"


func change_speed_multiplier():
	speed_multiplier = speed_factor * current_level


func reset():
	bricks.queue_free()
	score = 0
	current_level = 0
	lives_remaining = 3
	speed_multiplier = 0.0
	game_over = false
	# To reset the UI
	add_score(0)
	load_level()


func load_level():
	# If you pass, you get three lives for the next level.
	lives_remaining = 3
	draw_lives_remaining()
	
	bricks = bricks_prefab.instance()
	add_child(bricks)
	bricks.connect("brick_broke", self, "_on_Bricks_brick_broke")
	bricks.connect("last_brick_broke", self, "_on_Bricks_last_brick_broke")

	$UI/Level.text = str(current_level)
	$Player1.reset_ball()
	change_speed_multiplier()


func _physics_process(_delta):
	if Input.is_action_just_pressed("p1_launch"):
		if game_over:
			reset()
			
		elif $Player1.can_launch():
			ball = $Player1.launch_ball(ball_prefab)
			add_child(ball)


func _on_Bottom_body_entered(_body):
	lives_remaining -= 1
	draw_lives_remaining()
	ball.queue_free()
	
	if lives_remaining >= 0:
		$Player1.reset_ball()
	else:
		game_over = true


func _on_Bricks_brick_broke(row: int):
	add_score(row + 1)


func _on_Bricks_last_brick_broke():
	ball.queue_free()
	level_end()
