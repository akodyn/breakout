extends StaticBody2D
class_name Brick

export var row := 0
export var color: Color = Color.white
signal broke


func _ready():
	$ColorRect.color = color


func break_brick():
	emit_signal("broke", row)
	queue_free()
