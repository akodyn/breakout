extends KinematicBody2D

export var player := 1
export var speed := 800
var _can_launch := true


func launch_ball(var prefab):
	var ball = prefab.instance()
	ball.global_position = global_position + Vector2(0, -20)
	_can_launch = false
	$Ball.visible = false
	return ball


func reset_ball():
	$Ball.visible = true
	_can_launch = true


func can_launch():
	return _can_launch


func _physics_process(_delta):
	var velocity = Vector2.ZERO
	
	if player == 1:
		if Input.is_action_pressed("p1_left"):
			velocity.x = -speed * Input.get_action_strength("p1_left")
		elif Input.is_action_pressed("p1_right"):
			velocity.x = speed * Input.get_action_strength("p1_right")
	
	velocity = move_and_slide(velocity)
